
// #ifdef MP-ALIPAY
const storageSet = (key, value) => { // 设置本地存储  set
	uni.setStorage({
	    key,
	    value
	});
}

const storageGet = (key) => { // 获取本地存储  get
	uni.getStorage({
	    key: 'storage_key',
	    success: function (res) {
	        return res.data
	    }
	});
}

const storageRemove = (key) => { // 获取本地存储  get
	uni.removeStorage({
	    key
	});
}
// #endif
// #ifndef MP-ALIPAY
const storageSet = (key, value) => { // 设置本地存储  set
	try {
		uni.setStorageSync(key, value);
	} catch (e) {
		// error
	}
}

const storageGet = (key) => { // 获取本地存储  get
	try {
		return uni.getStorageSync(key);
	} catch (e) {
		// error
	}
}

const storageRemove = (key) => { // 获取本地存储  get
	try {
		return  uni.removeStorageSync(key);
	} catch (e) {
		// error
	}
}
// #endif



const TOKEN = 'token'
const USER_INFO = 'userInfo'

// token
export const setToken = (token) => {
	return storageSet(TOKEN, token)
}
export const getToken = () => {
	return storageGet(TOKEN)
}
export const removeToken = () => {
	return storageRemove(TOKEN)
}

// USER_INFO
export const setUserInfo = (data) => {
	return storageSet(USER_INFO, data)
}
export const getUserInfo = () => {
	return storageGet(USER_INFO)
}