// 更新消毒地址
'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
  const res = db.collection('distinct').where({
  	_id: event._id
  }).update({
		location: event.location,
		locationAddress: event.locationAddress,
		time: event.time
	})
  return res
};
