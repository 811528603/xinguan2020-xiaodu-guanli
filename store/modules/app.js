export default {
	namespaced: true,
	state: {
		bottom: 0
	},
	getters: {},
	mutations: {
		SET_BOTTOM(state, bool) {
			if(bool) {
				state.bottom = 60
			}
		}
	},
	actions: {
		phoneUs() { //联系我们
			uni.makePhoneCall({
			    phoneNumber: '17603036008'
			});
		},
		updateXcx({ // 更新小程序
			state,
			commit,
			dispatch
		}) {
			const updateManager = uni.getUpdateManager();

			updateManager.onCheckForUpdate(function(res) {
				// 请求完新版本信息的回调
				console.log(res.hasUpdate);
			});

			updateManager.onUpdateReady(function(res) {
				uni.showModal({
					title: '更新提示',
					content: '发现新版本，是否重启应用？',
					success(res) {
						if (res.confirm) {
							// 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
							updateManager.applyUpdate();
						}
					}
				});
			});
			updateManager.onUpdateFailed(function(res) {
				// 新的版本下载失败
			});
		},
		checkIsPhone11({ // 检查是否是苹果11 x
			state,
			commit,
			dispatch
		}) {
			uni.getSystemInfo({
				success(res) {
					if (res.model.search('iPhone X') != -1 || res.model.search('unknown') != -1) {
						commit("SET_BOTTOM", true)
					}
				}
			});
		}
	}
}
