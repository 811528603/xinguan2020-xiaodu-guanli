import {
	setUserInfo,
	getUserInfo
} from "@/utils/mini.js"
export default {
	namespaced: true,
	state: {
		userInfo: getUserInfo() || null
	},
	getters: {},
	mutations: {
		SET_USERINFO(state, userInfo) {
			state.userInfo = userInfo;
			setUserInfo(userInfo)
		},
	},
	actions: {
		login({
			state,
			commit,
			dispatch
		}, data) {
			uni.showLoading({
				title: '请稍后',
				mask: false
			});
			uniCloud.callFunction({
			  name: 'login',
			  data
			}).then(async (res) => {
				uni.hideLoading();
				let { code, data, message} = res.result
				if(code == 0) {
					await commit('SET_USERINFO',data)
					uni.switchTab({
					    url: '/pages/tabbar/list'
					});
				} else {
					uni.showToast({
					    title: message,
						icon: 'none',
					    duration: 2000
					});
				}
				console.log(res);
			})
		}

	}
}
